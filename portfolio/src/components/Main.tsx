import React, { useState, useEffect } from "react";
import { UserProps } from "../App";

const Main = () => {
  const [info, setInfo] = useState<UserProps | null>(null);
  const [isLoading, setLoading] = useState(false);

  const getAllInfos = async () => {
    const res = await fetch("https://api.github.com/users/MatheusMA678");
    const data = await res.json();

    const { name } = data;

    const userData: UserProps = {
      name,
    };

    setInfo(userData);
  };

  useEffect(() => {
    getAllInfos();
  }, []);

  return (
    <main>
      <section className="h-[90vh] flex flex-col gap-2 p-8">
        <h1 className="text-4xl font-semibold relative after:w-20 after:h-1 after:absolute after:left-0 after:-bottom-1 after:bg-blue-400 after:rounded-lg h-fit">
          Sobre Mim
        </h1>
        <section className="h-full">
          <p>{info && info.name}</p>
        </section>
      </section>
      <section className="h-screen">
        <h1>Projetos</h1>
      </section>
      <section className="h-screen">
        <h1>Contato</h1>
      </section>
    </main>
  );
};

export default Main;
