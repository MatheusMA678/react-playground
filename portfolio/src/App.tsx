import React, { useState } from "react";
import "./App.css";
import Main from "./components/Main";
import { IoClose, IoMenu } from "react-icons/io5";

export interface UserProps {
  name: string;
}

const App: React.FC = () => {
  const [show, setShow] = useState(false);

  return (
    <div className="dark:bg-gray-900 dark:text-white bg-gray-200 text-gray-900 min-h-screen scroll-smooth">
      <header>
        <div className="logo">
          <h1>Portfolio</h1>
        </div>
        <nav className="navigation-header hidden sm:block">
          <ul>
            <li>
              <a href="/">Sobre Mim</a>
            </li>
            <li>
              <a href="/">Projetos</a>
            </li>
            <li>
              <a href="/">Contato</a>
            </li>
          </ul>
        </nav>
        <button onClick={() => setShow(!show)} className="sm:hidden">
          <IoMenu size={28} />
        </button>
      </header>
      <div
        className={`w-full h-screen fixed top-0 bg-black/20 z-20 ${
          show ? "hidden" : "block"
        }`}
      >
        <div className="w-80 h-full dark:bg-gray-800 bg-white absolute right-0">
          <button onClick={() => setShow(!show)}>
            <IoClose size={28} />
          </button>
          <nav>
            <ul className="flex flex-col gap-2 pt-16 px-8 sidebar">
              <li>
                <a href="/">Sobre Mim</a>
              </li>
              <li>
                <a href="/">Projetos</a>
              </li>
              <li>
                <a href="/">Contato</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <Main />
    </div>
  );
};

export default App;
