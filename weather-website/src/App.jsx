import React, { useEffect, useState } from "react";
import { HiLocationMarker } from "react-icons/hi";
import { HiMagnifyingGlass } from "react-icons/hi2";
import Main from "./components/Main";
import "./App.css";

const api_key = "0406c195b96e0c244b5135fcd5656068";

import CloudSVG from "./assets/weather.svg";

const App = () => {
  const [weatherInfo, setWeatherInfo] = useState(null);
  const [city, setCity] = useState("");
  const [location, setLocation] = useState(null);

  const handleChange = (e) => {
    setCity(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${api_key}&lang=pt_br`
    )
      .then((res) => res.json())
      .then((data) => setWeatherInfo(data))
      .catch((err) => console.error(err));

    console.log(weatherInfo);
  };

  navigator.geolocation.getCurrentPosition(
    function success(position) {
      setLocation({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      });
    },
    function error() {
      return null;
    }
  );

  if (location !== null) {
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?lat=${location.latitude}&lon=${location.longitude}&units=metric&appid=${api_key}&lang=pt_br`
    )
      .then((res) => res.json())
      .then((data) => setWeatherInfo(data))
      .catch((err) => console.error(err));
  }

  // console.log(location);

  return (
    <div className="grid grid-cols-[40vw_auto_auto] grid-rows-2 gap-6 p-20 h-screen bg-clouds font-lato text-white">
      <section className="card row-span-2 relative">
        <img
          className="absolute -top-16 -left-16 w-36"
          src={
            weatherInfo
              ? `https://openweathermap.org/img/wn/${weatherInfo.weather[0].icon}.png`
              : CloudSVG
          }
          alt="weather icon"
        />
        <div className="absolute top-6 right-8 flex items-center gap-2 text-[#C2BFF4] font-bold text-sm">
          <HiLocationMarker />
          <span>{weatherInfo ? weatherInfo.name : "Rio do Sul, SC"}</span>
        </div>
        <div className="w-full h-full flex flex-col items-center p-4">
          <div className="flex-1 flex flex-col items-center justify-center">
            <h1 className="font-bold text-[88px] relative">
              {weatherInfo ? parseInt(weatherInfo.main.temp) : 18}
              <span className="absolute left-full top-6 text-2xl font-bold text-[#C2BFF4]">
                ºC
              </span>
            </h1>
            <div className="flex items-center gap-2 font-bold text-xl">
              <span>
                {weatherInfo ? parseInt(weatherInfo.main.temp_max) : 22}º
              </span>
              <span className="text-[#C2BFF4]">
                {weatherInfo ? parseInt(weatherInfo.main.temp_min) : 16}º
              </span>
            </div>
          </div>
          <form onSubmit={handleSubmit} className="bottom-card-1">
            <input
              type="text"
              onChange={handleChange}
              value={city}
              placeholder="Digite uma cidade"
            />
            <button
              type="submit"
              className="px-4 h-full outline-none transition active:bg-purple-800"
            >
              <HiMagnifyingGlass size={22} />
            </button>
          </form>
        </div>
      </section>
      <section className="card"></section>
      <section className="card"></section>
      <section className="card col-span-2 "></section>
    </div>
  );
};

export default App;
