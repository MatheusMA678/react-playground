import React, { useState } from "react";

const api_key = "0406c195b96e0c244b5135fcd5656068";

const Main = () => {
  const [weatherInfo, setWeatherInfo] = useState(null);
  const [value, setValue] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${value}&units=metric&appid=${api_key}&lang=pt_br`
    )
      .then((res) => res.json())
      .then((data) => setWeatherInfo(data))
      .catch((err) => console.error(err));
  };

  return (
    <div className="w-[500px] h-3/4 shadow-2xl rounded-lg p-8 flex flex-col items-center gap-4 bg-white">
      <h1 className="text-4xl font-semibold">Clima</h1>
      <form
        onSubmit={handleSubmit}
        className="flex w-64 h-10 rounded-lg border border-gray-500 shadow-lg overflow-hidden"
      >
        <input
          className="outline-none border-none h-full w-48 flex-1 px-2"
          type="text"
          onChange={(e) => setValue(e.target.value)}
          value={value}
        />
        <button
          className="outline-none border-none h-full px-2 flex-1 bg-gray-300"
          type="submit"
        >
          Enviar
        </button>
      </form>
      {weatherInfo ? (
        <div className="flex-1 flex flex-col gap-2 items-center justify-center">
          <h1 className="text-xl font-semibold">{weatherInfo.name}</h1>
          <p>{parseInt(weatherInfo.main.temp)}ºC</p>
          <div className="flex gap-2 items-center">
            <img
              className="h-12"
              src={`https://openweathermap.org/img/wn/${weatherInfo.weather[0].icon}.png`}
              alt={weatherInfo.weather[0].description}
            />
            <p>{weatherInfo.weather[0].description}</p>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default Main;
