/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    backgroundImage: {
      clouds: "url(./src/assets/bg.png)",
    },
    extend: {
      fontFamily: {
        lato: "Lato, sans-serif",
      },
    },
  },
  plugins: [],
};
